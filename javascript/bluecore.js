"use strict";
/////////////////////// BlueCore 1.7.2 \\\\\\\\\\\\\\\\\\\\\\\\\\

function getData(url,dothis,tothis,type,body) {
	var DataRequest = new XMLHttpRequest();
	if (undefined == type) { type = "GET"; }
	if (undefined == body) { body = ""; }
        DataRequest.open(type, url, true);
	DataRequest.onreadystatechange = function () {
        	if ( (DataRequest.readyState === XMLHttpRequest.DONE) && (DataRequest.status === 200) ) {
			dothis(DataRequest.responseText,tothis);
		}
	};
        DataRequest.send(body);
}

function isEmpty(object) {
console.log(object);
console.log(typeof object);
	if ( "object" == typeof object ) {
		for ( var propery in object ) {
			return false;
		}
		return true;
	} else {
		return false;
	}
}

function addJavascript(string,action) {
	try { 
		window.eval(string);
		if ("function" == typeof action) { action(); }
	}
	catch (e) { console.log("Error with external script: "+e); }
}

function getJavascript(url,action) {
	getData(url,addJavascript,action);
}

function addHTMLfrag(string,target) {
	target.innerHTML = string;
}

function getHTMLfrag(url,targetNode) {
	var target = document.querySelector(targetNode);
	target.innerHTML = "Loading...";
	getData(url,addHTMLfrag,target);
}

function elementMake (ID,Class,element) { //new and improved, I can use more than just divs for things now.
        return (function (myElement) {
                if ("string" == typeof ID) { myElement.id = ID; }
                if ("string" == typeof Class) { myElement.className = Class; }
                return myElement;
        })(document.createElement(element));
}

function elementPlace (parentID, ID, Class, element, position) {
	var newElement = elementMake(ID,Class,element);
	if ( (typeof document.querySelector(parentID).append && typeof document.querySelector(parentID).prepend) !== "undefined") { // Are we compliant?
		if ("before" == position) {
			document.querySelector(parentID).prepend(newElement);
		} else {
			document.querySelector(parentID).append(newElement);
		}
	} else {						//No? Ok we will use the old way.
		if ("before" == position) {
			var p = document.querySelector(parentID);
			p.insertBefore(newElement,p.firstChild);
		} else {
			document.querySelector(parentID).appendChild(newElement);
		}
	}
	return newElement;
}

function buttonAdd (ParentID,ID,Label,Action,Class,Element) {
        if ( "undefined" == typeof Class ) { Class = ""; }
	if ( "undefined" == typeof Element) { Element = "div"; }
        (function (button) {
                button.innerHTML = Label;
                button.onclick = function () { Action(); }
        })(elementPlace(ParentID,ID,"button "+Class,Element));
}

function dialogControlsGen(parentDialog,DialogBack,type,returnVar) { //I need to refactor this, these if statments are too similar.
        if ( "sub_window" == type ) {
                var closeButton = elementPlace(parentDialog,"closeButton",null,"div"); //Also need to change this, to help standardize the look and feel of the UI
                closeButton.innerHTML = "X";
                closeButton.onclick = function () {  document.getElementById("body").removeChild(DialogBack); }
                //More controls will go here probably as I flesh out the sub window design
        }
	if ( "input" == type ) {
		var inputArea = elementPlace(parentDialog,"listID","inputField","input",null);
		var submitButton = buttonAdd(parentDialog,"SubmitButton","Submit",function () { 
			returnVar.value = inputArea.value;
			document.getElementById("body").removeChild(DialogBack); 
		},"UI UIbutton");
	}
	if ( "input_password" == type ) {
		var inputArea = elementPlace(parentDialog,"listID","inputField","input",null);
		inputArea.type = "password";
		var submitButton = buttonAdd(parentDialog,"SubmitButton","Submit",function () { 
			returnVar.value = inputArea.value;
			document.getElementById("body").removeChild(DialogBack); 
		},"UI UIbutton");
	}
        if ( "info" == type ) {
                var closeButton = elementPlace(parentDialog,"closeButton",null,"div");
                closeButton.innerHTML = "Close";
                closeButton.onclick = function () {  document.getElementById("body").removeChild(DialogBack); }
        }
        if ( "end" == type ) {
                var closeButton = elementPlace(parentDialog,"closeButton",null,"div");
                closeButton.innerHTML = "Play Again";
                closeButton.onclick = function () {  document.getElementById("body").removeChild(DialogBack); gameInit(); }
        }
}

function dialogMessage (content,type,returnVar) {
        var message = elementPlace("#body","app_message_back","app_Message_Back","div");
        var dialog = elementPlace("#app_message_back","app_message","app_Message","div");
        dialog.innerHTML = content;
        dialogControlsGen("#app_message",message, type, returnVar);
}
//////////////////////////////////////////////////////////////
/*    */        /*    */        /*    */        /*    */       
        /*    */        /*    */        /*    */        /*  */
/*    */        /*    */        /*    */        /*    */       
/////////////////////// Gamers World UI \\\\\\\\\\\\\\\\\\\\\\

/*
Structure for Master list items
[ { "name":"", "URL":"", "content":"", "imagePath":"", "displayType":"", "genra":"", "datePublished":"" },  ... ]

Structure for list items
[ { "name":"","URL":"", "imagePath":"" }, ... ]

Structure for slide items
[ { "name":"","URL":"","content":"","imagePath":"","displayType":"" }, ... ]
*/

function addSpace(text) {
	return text.replace(/_/g," ");
}

function getList(listObject,sortingAttribute,sortingValue) {
	return listObject.filter(function (item) { return item[sortingAttribute] == sortingValue; });
}

function getTopList(listObject,sortingAttribute,direction,prefunction) {
	if ("string" != typeof direction) { direction = "dec"; }
	if ( (direction != "dec") && (direction != "asc") ) { direction = "dec"; }
	if ("function" != typeof prefunction) { prefunction = function (item) { return item; } }
	var mapped = listObject.map(function (item, index) {
		return { "index":index, "value":item[sortingAttribute] }; 
	});
	mapped.sort(function (a,b) {
		var one = prefunction(a.value);
		var two = prefunction(b.value);
		if (direction == "asc") {
			if (one > two) { return 1; }
			if (one < two) { return -1; }
			return 0;
		} else if (direction == "dec") {
			if (one > two) { return -1; }
			if (one < two) { return 1; }
			return 0;	
		}
	});
	return mapped.map( function (item) {
		return listObject[item.index]
	});
}

function linkProcess (URL,target,scriptBool,scriptTarget) {
	if ("string" != typeof target) { target = "#content"; }
	if ("boolean" != typeof scriptBool) { scriptBool = true; }
	if ("string" != typeof scriptTarget) { scriptTarget = "#pagescript"; }
	window.lastCalledURL = URL;
//	getJavascript("./games.json");
//	getJavascript("./newsbits.json");
	getData(URL, function (string) {
		var pageTarget = document.querySelector(target);
		addHTMLfrag(string,pageTarget);
		if (scriptBool && pageTarget.querySelector(scriptTarget)) { addJavascript(pageTarget.querySelector(scriptTarget).innerText) };
	});
}

function buildIconMenu (listName,itemList,targetPageArea) {
	if ("string" != typeof targetPageArea) { targetPageArea = "#content"; }
	var listContainer = elementPlace("#"+listName,listName+"_list","card list horizontal","ul");
	itemList.forEach(function (item,index) {
		var currentItem = elementPlace("#"+listName+"_list",null,"card icon","li");
		currentItem.onclick = function () { window.gameplayer.game = item; linkProcess(item.URL,targetPageArea); };
		currentItem.style.backgroundImage = item.imagePath;
	});
}

function buildCardMenu (listName,itemList,targetPageArea) {
	if ("string" != typeof targetPageArea) { targetPageArea = "#content"; }
	elementPlace("#"+listName,listName+"_list","card list virtical","ul");
	itemList.forEach(function (item,index) {
		var currentItem = elementPlace("#"+listName+"_list",null,"card slot","li");
		currentItem.onclick = function () { window.gameplayer.game = item; linkProcess(item.URL,targetPageArea); };
		currentItem.style.backgroundImage = item.imagePath;
		currentItem.innerHTML = '<p>'+item.name+'</p>';
	});
}

function buildTextMenu (listName,itemList,targetPageArea) {
	if ("string" != typeof targetPageArea) { targetPageArea = "#content"; }
	elementPlace("#"+listName,listName+"_list","card list virtical","ul");
	itemList.forEach(function (item,index) {
		var currentItem = elementPlace("#"+listName+"_list",null,"card text","li");
		currentItem.onclick = function () { window.gameplayer.game = item; linkProcess(item.URL,targetPageArea); };
		currentItem.innerHTML = '<p>'+item.name+'</p>';
	});
}

function buildMenu (parentID,listName,listType,itemList,targetPageArea) {
	if ("string" != typeof targetPageArea) { targetPageArea = "#content"; }
	if (typeof itemList != "object") { itemList = {}; }
	var container = elementPlace(parentID,listName,"card container","div");
	container.titleName = elementPlace("#"+listName,null,"card title","div");
	container.titleName.innerText = addSpace(listName);
	if (listType == "Icon") { buildIconMenu(listName,itemList,targetPageArea); 
	} else if (listType == "Card") { buildCardMenu(listName,itemList,targetPageArea); 
	} else if (listType == "Text") { buildTextMenu(listName,itemList,targetPageArea); 
	} else { console.log("Error: incorrect or no list type specified."); }
}

function sliderChange (slider, nextSlide) {
	var currentIndex = slider.slides.findIndex( function (slide) { return slide.current;});
	var current = slider.slides[currentIndex];
	var currentPip = slider.controls.pips.pipArray[currentIndex];
	var next = slider.slides[nextSlide];
	var nextPip = slider.controls.pips.pipArray[nextSlide];
	current.current = false;
	current.className = "card slide inactive_slide";
	currentPip.className = "slide_pip";
	next.current = true;
	next.className = "card slide active_slide";
	nextPip.className += " pip_active";
}

function buildSlider (parentID, showName, slideList) {
	if (typeof slideList != "object") { slideList = {}; }
	var container = elementPlace(parentID,showName,"card container","div");
	container.titleName = elementPlace("#"+showName, null, "card title", "div");
	container.titleName.innerText = addSpace(showName);
	container.stage = elementPlace("#"+showName, showName+"_stage", "stage", "div");
	container.stage.slider = elementPlace("#"+showName+"_stage", showName+"_slider", "slider", "ul");
	var controlsName = showName+"_controls";
	container.controls = elementPlace("#"+showName, showName+"_controls", "slider_controls", "form");
	container.controls.pips = elementPlace("#"+controlsName, controlsName+"_pips", "pip_container", "ol");
	container.controls.pips.pipArray = [];
	container.slides = [];
	slideList.forEach(function (slide,index) {
		var slideName = showName+"_slide_"+index;
		var currentSlide = elementPlace("#"+showName+"_slider", slideName, "card slide", "li");
		currentSlide.name = elementPlace("#"+slideName, null, "slide_title slide_text", "p");
		if ( "string" == typeof(slide.imagePath) ) { 
			currentSlide.name.style.backgroundImage = slide.imagePath;
		}
		currentSlide.name.innerText = slide.name;
		currentSlide.content = elementPlace("#"+slideName, null, "slide_text slide_content", "div");
		currentSlide.content.innerHTML = slide.content;
		if ("string" == typeof(slide.URL)) {
			currentSlide.moreButton = buttonAdd("#"+slideName, null, "More", function () {
				linkProcess(slide.URL);
			}, "button", "div");
		}
		container.controls.pips.pipArray[index] = elementPlace("#"+controlsName+"_pips",null,"slide_pip","li");
		container.controls.pips.pipArray[index].onclick = function () { sliderChange(container,index); };
		currentSlide.className = "card slide inactive_slide";
		container.slides[index] = currentSlide;
		container.slides[index].current = false;
	});
	container.slides[0].className = "card slide active_slide"
	container.slides[0].current = true;
	container.controls.pips.pipArray[0].className += " pip_active";
	container.controls.backButton = buttonAdd("#"+controlsName, null, "<", function () {
		var currentIndex = container.slides.findIndex( function (slide) { return slide.current;});
		var nextIndex;
		if (0 == currentIndex) { 
			nextIndex = container.slides.length - 1;
		} else { nextIndex = currentIndex - 1; }
		sliderChange(container,nextIndex);
	}, "slide_control back", "div");
	container.controls.forwordButton = buttonAdd("#"+controlsName, null, ">", function () {
		var currentIndex = container.slides.findIndex( function (slide) { return slide.current;});
		var nextIndex;
		if (container.slides.length - 1  == currentIndex) { 
			nextIndex = 0;
		} else { nextIndex = currentIndex + 1; }
		sliderChange(container,nextIndex);
	}, "slide_control forword", "div");
}






